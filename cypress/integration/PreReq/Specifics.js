/// <reference types="cypress" />
import Generics from "./Generics"
class Specifics extends Generics
{
    //Login To The Application
    loginIntoApp(loc,data)
    {
        this.openURL(data.URL)
        this.input(loc.LoginPage.UserName,data.UserName)
        this.input(loc.LoginPage.Password,data.Password)
        this.LinkTextClick(loc.LoginPage.Terms)
        this.LinkTextClick(loc.LoginPage.Login)
    }

    //Account Verify
    AccountPage(loc)
    {
        this.click(loc.LoginPage.ParentAccount)
        this.click(loc.LoginPage.SelectAccount)
        this.LinkTextClick(loc.LoginPage.Account)
        cy.get(loc.LoginPage.AddNewAccount).should('be.visible')
        
    }
}
export default Specifics