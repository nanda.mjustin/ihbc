/// <reference types="cypress" />
class Generics
{

    //Open URL
    openURL(url)
    {
        cy.visit(url)
    }

    //Inputting The Data To Text Box
    input(location,data)
    {
        
        this.LocatorType(location).type(data).should("have.value",data)
    }

    //Clicking The Element
    click(location,data)
    {
        this.LocatorType(location).click()
    }

    //Clear Then Input The Element
    ClearThenInput(location,data)
    {
        this.LocatorType(location).clear().then().type(data).should("have.value",data)
    }

    //Clicking The Element Depends on inner text
    LinkTextClick(location)
    {
        cy.contains(location).click()
    }

    //Mouse Over if inner text is available
    MouseOverOnText(location)
    {
        cy.contains(location).trigger('mouseover')
    }

     //Mouse Over if inner text is not available
    MouseOver(location)
    {
        this.LocatorType(location).trigger('mouseover')
    }

    //Force Click - Hidden Elements
    ForceClick(location)
    {
        this.LocatorType(location).click({force: true})
    }

    //Mouse Over - When Inner Text Available
    MouseOverText(location)
    {
        cy.contains(location).trigger('mouseover')
    }

     //Mouse Over - When Text Available
    MouseOverText(location)
    {
        this.LocatorType(location).trigger('mouseover')
    }

    //Open New Tab in current window
    OpenNewTabInCurrentWindow(location)
    {
        this.LocatorType(location).invoke('removeAttr','target').click()
    }

    //Open New Tab in current window
    OpenNewTabInCurrentWindowBasedOnText(location)
    {
        cy.contains(location).invoke('removeAttr','target').click()
    }

    //Locator Type Will Be Returned
    LocatorType(data)
    {
        var loctype

       /* if(data.includes('//'))
        {
            loctype = cy.xpath(data)
        }
        else
        {*/
            loctype = cy.get(data)
       // }

        return loctype
    }

    //Drag And Drop
    DragAndDrop(data,loc)
    {
        this.LocatorType(data).drag(loc,{force:true})
        /*const dataTransfer = new DataTransfer()
        cy.xpath(data,{force: true}).trigger("dragstart",{dataTransfer},{force: true})
        cy.get(loc,{force: true}).trigger("drop",{dataTransfer},{force: true})
        cy.xpath(data,{force: true}).trigger("dragend",{dataTransfer},{force: true})*/
       // dndNative(data,loc,true)
    }

    //Drop Down Select
    SelectDropDown(loc,data)
    {
        this.LocatorType(loc).select(data)
    }

    //Finds The Length
    FindLength(data)
    {
         var count = Object.keys(data).length;
        return count
    }

    //Replace
    ReplaceString(data,data1)
    {
        var data = data.replace("[$]",data1)

        return data
    }

}
export default Generics